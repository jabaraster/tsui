{-# OPTIONS_GHC -fno-warn-orphans #-} -- for 'instance PersistUserCredentials User'
module Foundation where

import qualified Data.CaseInsensitive as CI
import qualified Data.Text.Encoding as TE

import           Database.Persist.Sql (ConnectionPool, runSqlPool)

import           Import.NoFoundation

import           Jabara.Util (commaS)
-- import           Jabara.Yesod.Auth.Simple (YesodAuthSimple(..), authSimplePlugin)
import           Jabara.Yesod.Auth.FreeAccount (YesodAuthFreeAccount(..), AccountSendEmail(..)
                 , AccountPersistDB, runAccountPersistDB
                 , authFreeAccountPlugin
--                  , verifyPassword
                 , PersistUserCredentials(..))
import           Jabara.Yesod.Middleware (csrfMiddleware)

import           Tsui.Mail

import           Text.Hamlet (hamletFile)
import           Text.Jasmine (minifym)

import           Yesod.Core.Types     (Logger)
import qualified Yesod.Core.Unsafe as Unsafe
import           Yesod.Default.Util   (addStaticContentExternal)

-- | The foundation datatype for your application. This can be a good place to
-- keep settings and values requiring initialization before your application
-- starts running, such as database connections. Every handler will have
-- access to the data present here.
data App = App
    { appSettings    :: AppSettings
    , appStatic      :: Static -- ^ Settings for static file serving.
    , appConnPool    :: ConnectionPool -- ^ Database connection pool.
    , appHttpManager :: Manager
    , appLogger      :: Logger
    }

-- This is where we define all of the routes in our application. For a full
-- explanation of the syntax, please see:
-- http://www.yesodweb.com/book/routing-and-handlers
--
-- Note that this is really half the story; in Application.hs, mkYesodDispatch
-- generates the rest of the code. Please see the following documentation
-- for an explanation for this split:
-- http://www.yesodweb.com/book/scaffolding-and-the-site-template#scaffolding-and-the-site-template_foundation_and_application_modules
--
-- This function also generates the following type synonyms:
-- type Handler = HandlerT App IO
-- type Widget = WidgetT App IO ()
mkYesodData "App" $(parseRoutesFile "config/routes")

-- | A convenient synonym for creating forms.
type Form x = Html -> MForm (HandlerT App IO) (FormResult x, Widget)

-- Please see the documentation for the Yesod typeclass. There are a number
-- of settings which can be configured by overriding methods here.
instance Yesod App where
    -- Controls the base of generated URLs. For more information on modifying,
    -- see: https://github.com/yesodweb/yesod/wiki/Overriding-approot
    approot = ApprootRequest $ \app req ->
        case appRoot $ appSettings app of
            Nothing -> getApprootText guessApproot app req
            Just root -> root

    makeSessionBackend _ = Just <$> defaultClientSessionBackend
        (24 * 60)    -- timeout in minutes
        "config/client_session_key.aes"

    -- Yesod Middleware allows you to run code before and after each handler function.
    -- The defaultYesodMiddleware adds the response header "Vary: Accept, Accept-Language" and performs authorization checks.
    -- The defaultCsrfMiddleware:
    --   a) Sets a cookie with a CSRF token in it.
    --   b) Validates that incoming write requests include that token in either a header or POST parameter.
    -- For details, see the CSRF documentation in the Yesod.Core.Handler module of the yesod-core package.
    yesodMiddleware = csrfMiddleware [AuthR LogoutR] . defaultYesodMiddleware

    defaultLayout widget = do
        master <- getYesod
        mmsg <- getMessage
        authenticated <- maybeAuthId
                             >>= return . maybe False (\_ -> True)

        -- We break up the default layout into two components:
        -- default-layout is the contents of the body tag, and
        -- default-layout-wrapper is the entire page. Since the final
        -- value passed to hamletToRepHtml cannot be a widget, this allows
        -- you to use normal widget features in default-layout.

        pc <- widgetToPageContent $ do
            addStylesheet $ StaticR css_bootstrap_css
            addStylesheet $ StaticR css_sweetalert_css
            addScript $ StaticR js_jquery_js
            addScript $ StaticR js_js_cookie_js
            addScript $ StaticR js_bootstrap_js
            addScript $ StaticR js_app_nav_js
            addScript $ StaticR js_sweetalert_js
            $(widgetFile "default-layout")
        withUrlRenderer $(hamletFile "templates/default-layout-wrapper.hamlet")

    -- The page to be redirected to when authentication is required.
    authRoute _ = Just $ AuthR LoginR

    isAuthorized (AuthR _) _ = return Authorized
    isAuthorized FaviconR _ = return Authorized
    isAuthorized RobotsR _ = return Authorized
    isAuthorized SandboxR _ = return Authorized
    isAuthorized HostInfoR _ = return Authorized
    isAuthorized _ _ = maybeAuthId >>= return . maybe AuthenticationRequired (\_ -> Authorized)

    -- This function creates static content files in the static folder
    -- and names them based on a hash of their content. This allows
    -- expiration dates to be set far in the future without worry of
    -- users receiving stale content.
    addStaticContent ext mime content = do
        master <- getYesod
        let staticDir = appStaticDir $ appSettings master
        addStaticContentExternal
            minifym
            genFileName
            staticDir
            (StaticR . flip StaticRoute [])
            ext
            mime
            content
      where
        -- Generate a unique filename based on the content itself
        genFileName lbs = "autogen-" ++ base64md5 lbs

    -- What messages should be logged. The following includes all messages when
    -- in development, and warnings and errors in production.
    shouldLog app _source level =
        appShouldLogAll (appSettings app)
            || level == LevelWarn
            || level == LevelError

    makeLogger = return . appLogger

-- How to run database actions.
instance YesodPersist App where
    type YesodPersistBackend App = SqlBackend
    runDB action = do
        master <- getYesod
        runSqlPool action $ appConnPool master
instance YesodPersistRunner App where
    getDBRunner = defaultGetDBRunner appConnPool

instance AccountSendEmail App where
    -- sendVerifyEmail :: Username
    --                 -> Text -- ^ email address
    --                 -> Text -- ^ verification URL
    --                 -> HandlerT master IO ()
    sendVerifyEmail uname email url = do
        msg <- return $ concat [ "Verification email for "
                              , uname
                              , " (", email, "): "
                              , url
                              ]
        $(logDebug) msg
        now    <- liftIO $ getCurrentTime
        _      <- runDB $ insert Activity { activityText = msg, activityTime = now }
        result <- liftIO $ sendSimpleMail SimpleMailData {
                    _simpleMailDataFrom = address "Admin" "ah@jabara.info"
                  , _simpleMailDataTo = address uname email
                  , _simpleMailDataSubject = "Verify emai address."
                  , _simpleMailDataBody = "Please accept this URL for email verification.\n" ++ url
                  }
        case result of
            Left err -> $(logDebug) (pack err)
            Right _  -> return ()

    -- sendNewPasswordEmail :: Username
    --                      -> Text -- ^ email address
    --                      -> Text -- ^ new password URL
    --                      -> HandlerT master IO ()
    sendNewPasswordEmail uname email url = do
        msg <- return $ concat [ "Reset password email for "
                              , uname
                              , " (", email, "): "
                              , url
                              ]
        $(logDebug) msg
        now    <- liftIO $ getCurrentTime
        _      <- runDB $ insert Activity { activityText = msg, activityTime = now }
        result <- liftIO $ sendSimpleMail SimpleMailData {
                    _simpleMailDataFrom = address "Admin" "ah@jabara.info"
                  , _simpleMailDataTo = address uname email
                  , _simpleMailDataSubject = "New password page"
                  , _simpleMailDataBody = "Please accept this URL for new password.\n" ++ url
                  }
        case result of
            Left err -> $(logDebug) (pack err)
            Right _  -> return ()

instance YesodAuthFreeAccount (AccountPersistDB App User) App where
    runAccountDB = runAccountPersistDB

{--
instance YesodAuthSimple App where
    doesUserNameExist userName = do
        mUser <- runDB $ getBy $ UniqueUser userName
        case mUser of
          Nothing -> pure False
          Just _  -> pure True
    validatePassword userName password = do
        mUser <- runDB $ getBy $ UniqueUser userName
        case mUser of
            Nothing   -> pure False
            Just user -> pure $ verifyPassword password  (userPassword $ rec' user)
      where
        rec' :: Entity a -> a
        rec' (Entity _ a) = a
--}

instance PersistUserCredentials User where
    userUsernameF = UserName
    userPasswordHashF = UserPassword
    userEmailF = UserEmailAddress
    userEmailVerifiedF = UserVerified
    userEmailVerifyKeyF = UserVerifyKey
    userResetPwdKeyF = UserResetPasswordKey
    uniqueUsername = UniqueUser
    userCreate name email key pwd = User name pwd email False key ""

instance YesodAuth App where
    type AuthId App = UserId

    -- Where to send a user after successful login
    loginDest _ = HomeR
    -- Where to send a user after logout
    logoutDest _ = AuthR LoginR
    -- Override the above two destinations when a Referer: header is present
    redirectToReferer _ = True

    authenticate creds = runDB $ do
        x <- getBy $ UniqueUser $ credsIdent creds
        case x of
            Just (Entity uid _) -> return $ Authenticated uid
            Nothing -> Authenticated <$> insert User
                {userName = "hoge"
                ,userPassword = "password"
                ,userEmailAddress = ""
                ,userVerified = False
                ,userVerifyKey = ""
                ,userResetPasswordKey = ""
                }
    -- You can add other plugins like BrowserID, email or OAuth here
    authPlugins _ = [authFreeAccountPlugin {-- , authSimplePlugin --}]

    authHttpManager = getHttpManager

    onLogin = setMessage "ログインしました"

instance YesodAuthPersist App

-- This instance is required to use forms. You can modify renderMessage to
-- achieve customized and internationalized form validation messages.
instance RenderMessage App FormMessage where
    renderMessage _ _ = defaultFormMessage

-- Useful when writing code that is re-usable outside of the Handler context.
-- An example is background jobs that send email.
-- This can also be useful for writing code that works across multiple Yesod applications.
instance HasHttpManager App where
    getHttpManager = appHttpManager

unsafeHandler :: App -> Handler a -> IO a
unsafeHandler = Unsafe.fakeHandlerGetLogger appLogger

c :: Show a => a -> String
c = commaS

-- Note: Some functionality previously present in the scaffolding has been
-- moved to documentation in the Wiki. Following are some hopefully helpful
-- links:
--
-- https://github.com/yesodweb/yesod/wiki/Sending-email
-- https://github.com/yesodweb/yesod/wiki/Serve-static-files-from-a-separate-domain
-- https://github.com/yesodweb/yesod/wiki/i18n-messages-in-the-scaffolding
