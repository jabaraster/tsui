module Handler.CostParameters where

import Import

import Jabara.Persist.Util (toKey, toRecord)

getCostParametersEditR :: Handler Html
getCostParametersEditR = defaultLayout $ do
    addScript $ StaticR js_cost_parameters_edit_js
    setTitle' "原価パラメータを編集"
    $(widgetFile "cost-parameters-edit")

getCostParametersR :: Handler CostParameters
getCostParametersR = runDB $ getCostParameters >>= return . toRecord

postCostParametersR :: Handler GenericJsonResponse
postCostParametersR =  do
    req  <- requireJsonBody :: Handler CostParameters
    runDB $ do
      inDb <- getCostParameters
      replace (toKey inDb) req
    return $ SuccessDialog "保存しました！"
