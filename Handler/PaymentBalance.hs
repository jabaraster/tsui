module Handler.PaymentBalance where

import Import
import Control.Lens((^.), (&), (.~))
import Jabara.Persist.Util (toKey)
import Jabara.Yesod.Util (getResourcePath)

getPaymentBalanceIndexR :: Handler PaymentIndecies
getPaymentBalanceIndexR = do
    idc <- runDB $ getPaymentIndecies
    mapM (\idx -> do
                   path <- getResourcePath $ SchoolEditR $ toKey $ idx^.paymentIndexSchool
                   pure $ idx&paymentIndexSchoolEditUrl .~ path
         ) idc

