{-# LANGUAGE OverloadedStrings #-}
module Handler.Sandbox where

import Import
import Jabara.Util.Network (localHostAddress)

getSandboxR :: Handler Html
getSandboxR = defaultLayout $ do
    addScript $ StaticR js_sandbox_js
    setTitle' "サンドボックス"
    $(widgetFile "sandbox")

getHostInfoR :: Handler Html
getHostInfoR = defaultLayout $ do
    addr <- liftIO localHostAddress
    setTitle' "ホスト情報"
    $(widgetFile "host-info")
