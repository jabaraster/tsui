module Handler.School where

import           Import

import           Control.Lens ((^.))

import qualified Data.Map as Map
import qualified Data.Maybe as Maybe

import           Jabara.Persist.Util (toKey, toRecord, dummyKey)
import           Jabara.Yesod.Util (getResourcePath)

getAllSchoolInstallations :: MonadIO m => ReaderT SqlBackend m [SI]
getAllSchoolInstallations = do
    schools       <- selectList [] [Asc SchoolId]
    installations <- selectList [InstallationSchoolId <-. (map toKey schools)] []
                         >>= pure . _conv
    pure $ map (_collect installations) schools
  where
    _conv :: [Entity Installation] -> Map (Key School) Installation
    _conv = Map.fromList . map (\se -> (installationSchoolId $ toRecord se, toRecord se))

    _collect :: Map (Key School) Installation -> (Entity School) -> SI
    _collect installations school =
        let schoolId     = toKey school
            installation = Maybe.fromJust $ Map.lookup schoolId installations
        in
            SI (toRecord school) installation []

getSchoolIndexR :: Handler Html
getSchoolIndexR = do
    schools <- runDB $ getAllSchoolInstallations
    defaultLayout $ do
        addScript $ StaticR js_school_index_js
        setTitle' "学校一覧"
        $(widgetFile "school-index")

putSchoolIndexR :: Handler GenericJsonResponse
putSchoolIndexR = do
    req <- requireJsonBody :: Handler SI
    if (schoolName $ _siSchool req) == "" then
        pure $ ErrorDialog "学校名を入力して下さい。"
      else
        runDB $ _core req
  where
    _core req = do
        m <- checkUnique $ req^.siSchool
        case m of
            Just _  -> pure $ ErrorDialog "学校名が重複しています。"
            Nothing -> do
                schoolId <- do
                    schoolId <- insert $ req^.siSchool
                    insert_ $ (req^.siInstallation) {
                                 installationSchoolId = schoolId }
                    insertEntries req schoolId
                    pure schoolId
                path <- getResourcePath $ SchoolEditR schoolId
                pure $ DialogRedirect "登録しました！" path

getSchoolIndexJsonR :: Handler [Entity School]
getSchoolIndexJsonR = runDB $ selectList [] []

insertEntries :: MonadIO m => SI -> Key School -> ReaderT SqlBackend m ()
insertEntries si schoolId = insertMany_ $ map (\e -> e { entrySchoolId = schoolId }) (si^.siEntries)

getSchoolNewR :: Handler Html
getSchoolNewR = defaultLayout $ do
    schoolId <- pure dummyKey
    addScript $ StaticR js_school_edit_js
    setTitle' "新しい学校"
    $(widgetFile "school-edit")

getSchoolEditR :: SchoolId -> Handler Html
getSchoolEditR schoolId = do
    _ <- runDB $ get404 schoolId
    defaultLayout $ do
        addScript $ StaticR js_school_edit_js
        setTitle' "学校を編集"
        $(widgetFile "school-edit")

getSchoolR :: SchoolId -> Handler SI
getSchoolR schoolId = do
    runDB $ do
      school <- get404 schoolId
      installation <- getBy404 (UniqueInstallation schoolId)
      entries      <- selectList [EntrySchoolId ==. schoolId] [Asc EntrySchoolYear]
      pure $ SI school (toRecord installation) (map toRecord entries)

postSchoolR :: SchoolId -> Handler GenericJsonResponse
postSchoolR schoolId = do
    req <- requireJsonBody :: Handler SI
    if (schoolName $ req^.siSchool) == "" then
        pure $ ErrorDialog "学校名を入力して下さい。"
      else
        runDB $ _core req
  where
    _core req = do
        m <- selectFirst [SchoolId !=. schoolId, SchoolName ==. (schoolName $ _siSchool req)] []
        case m of
            Just _  -> pure $ ErrorDialog "学校名が重複しています。"
            Nothing -> do
                inst <- getBy404 (UniqueInstallation schoolId)
                replace schoolId (req^.siSchool)
                replace (toKey inst) (req^.siInstallation)
                deleteWhere [EntrySchoolId ==. schoolId]
                insertEntries req schoolId
                pure $ SuccessDialog "更新しました！"

deleteSchoolR :: SchoolId -> Handler GenericJsonResponse
deleteSchoolR schoolId = do
    runDB $ do
        deleteWhere [InstallationSchoolId ==. schoolId]
        deleteWhere [EntrySchoolId ==. schoolId]
        delete schoolId
    path <- getResourcePath SchoolIndexR
    pure $ Redirect path

