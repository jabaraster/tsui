module Handler.TagExpense where

import Import
import Control.Lens((^.), (&), (.~))
import Jabara.Persist.Util (toKey)
import Jabara.Yesod.Util (getResourcePath)
import Text.Read (readMaybe)

getTagExpenseIndexR :: Handler TagExpenseIndecies
getTagExpenseIndexR = do
    sys <- lookupGetParams "schoolYear"
             >>= pure . sort . catMaybes . map (readMaybe . unpack)
    idc <- runDB $ getTagExpenseIndecies sys
    mapM (\idx -> do
                    path <- getResourcePath $ TagOrderEditR
                                $ toKey $ idx^.tagExpenseIndexTagOrder
                    pure $ idx&tagExpenseIndexTagOrderEditUrl .~ path
         ) idc
