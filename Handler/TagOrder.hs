module Handler.TagOrder where

import Import
import Jabara.Persist.Util (dummyKey)
import Jabara.Yesod.Util (getResourcePath)

getTagOrderIndexR :: Handler Html
getTagOrderIndexR = do
    tagOrders <- runDB $ selectList [] []
    defaultLayout $ do
        addScript $ StaticR js_tag_order_index_js
        setTitle' "ICタグ購入履歴"
        $(widgetFile "tag-order-index")

putTagOrderIndexR :: Handler GenericJsonResponse
putTagOrderIndexR = do
    req   <- requireJsonBody :: Handler TagOrder
    if tagOrderCount req <= 0 then
        return $ ErrorDialog "タグの個数を入力して下さい。"
      else if tagOrderTotalPrice req <= 0 then
        return $ ErrorDialog "代金を入力して下さい。"
      else do
        _     <- runDB $ insert req
        return $ SuccessDialog "登録しました！"

getTagOrderNewR :: Handler Html
getTagOrderNewR = defaultLayout $ do
    tagOrderId <- return dummyKey
    setTitle' "ICタグ履歴を登録"
    addScript $ StaticR js_tag_order_edit_js
    $(widgetFile "tag-order-edit")

getTagOrderEditR :: TagOrderId -> Handler Html
getTagOrderEditR tagOrderId = do
    _ <- runDB $ get404 tagOrderId
    defaultLayout $ do
        setTitle' "ICタグ履歴を訂正"
        addScript $ StaticR js_tag_order_edit_js
        $(widgetFile "tag-order-edit")

getTagOrderR :: TagOrderId -> Handler TagOrder
getTagOrderR tagOrderId = runDB $ get404 tagOrderId

postTagOrderR :: TagOrderId -> Handler GenericJsonResponse
postTagOrderR tagOrderId = do
    req  <- requireJsonBody :: Handler TagOrder
    _    <- runDB $ replace tagOrderId req
    path <- getResourcePath TagOrderIndexR
    return $ DialogRedirect "変更を保存しました！" path

deleteTagOrderR :: TagOrderId -> Handler GenericJsonResponse
deleteTagOrderR tagOrderId = do
    runDB $ delete tagOrderId
    path <- getResourcePath TagOrderIndexR
    return $ Redirect path
