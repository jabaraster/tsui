module Handler.User where

import Import
import Database.Persist.Sql (fromSqlKey)

getUserIndexR :: Handler Html
getUserIndexR = do
    users <- runDB $ selectList [] []
    defaultLayout $ do
        setTitle' "ユーザー一覧"
        $(widgetFile "user-index")
