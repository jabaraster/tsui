# ツイタもん収支シミュレータ
React+Yesodの勉強のために作ってみている.


# デプロイ
## 前提
Ubuntu(Ubuntu 14.04.3 LTS)にデプロイすることにします.
(Amazon Linuxはポータビリティに欠けるので避けた)

Yesodを使っているのでketerを使う.
参考URL
https://github.com/commercialhaskell/stack/blob/master/doc/install_and_upgrade.md

## AWSを使う場合 Instance Typeに要注意.

t2.microではketerのインストール時にメモリが足りない.  
Yesodアプリのビルドにも、多分不足するだろう.  
1.5GBは欲しい.  

### タイムゾーンの変更
http://www.server-world.info/query?os=Ubuntu_14.04&p=timezone

```sh
sudo timedatectl set-timezone Asia/Tokyo
```

### ライブラリ最新化

```sh
sudo apt-get upgrade -y
```

### stackのインストール

```sh
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 575159689BEFB442
echo 'deb http://download.fpcomplete.com/ubuntu trusty main'|sudo tee /etc/apt/sources.list.d/fpco.list
sudo apt-get update && sudo apt-get install stack -y
```

### postgresのインストール

```sh
sudo apt-get install postgresql -y
sudo apt-get install libpq-dev -y
```

### keterのインストール
参考URL
https://github.com/snoyberg/keter

```sh
wget -O - https://raw.githubusercontent.com/snoyberg/keter/master/setup-keter.sh | bash
sudo cp /opt/keter/bin/keter /usr/bin/
```

### keter起動

```sh
sudo start keter
```



## ビルドサーバにする場合

### ソースのダウンロード

stackはインストール済みである前提.

```sh
git clone https://jabaraster@bitbucket.org/jabaraster/tsui-lib.git
git clone https://jabaraster@bitbucket.org/jabaraster/tsui.git
cd tsui
stack build
stack install yesod-bin
stack exec -- yesod keter
```


