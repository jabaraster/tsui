{-# LANGUAGE DeriveGeneric #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Tsui.Web (
    ResponseStatus(..)
    , GenericJsonResponse(..)
    , setTitle'
) where

import ClassyPrelude.Yesod
import Jabara.Yesod.Util (tc, ttc)
import Tsui.Model (CostParameters, TagOrder, School)
import Tsui.Types (SI, Cost, PaymentIndecies, TagExpenseIndecies)

setTitle' :: MonadWidget m => Html -> m ()
setTitle' title = setTitle (title ++ " - ついしみゅ. build on Yesod")

data ResponseStatus = OK | NG
    deriving (Read, Show, Eq, Generic)
instance ToJSON ResponseStatus
instance ToContent ResponseStatus where toContent = tc
instance ToTypedContent ResponseStatus where toTypedContent = ttc

data GenericJsonResponse = Redirect {
                               redirectPath::Text
                           }
                           | DialogRedirect {
                               dialogMessage::Text
                               , redirectPath::Text
                           }
                           | ErrorDialog {
                               dialogMessage::Text
                           }
                           | SuccessDialog {
                               dialogMessage::Text
                           }
                           | GenericResponse {
                               status::ResponseStatus
                               , message::Text
                           }
                           deriving (Read, Show, Eq, Generic)

instance ToJSON GenericJsonResponse
instance ToContent GenericJsonResponse where toContent = tc
instance ToTypedContent GenericJsonResponse where toTypedContent = ttc

instance ToContent CostParameters where toContent = tc
instance ToTypedContent CostParameters where toTypedContent = ttc

instance ToContent TagOrder where toContent = tc
instance ToTypedContent TagOrder where toTypedContent = ttc

instance ToContent SI where toContent = tc
instance ToTypedContent SI where toTypedContent = ttc

instance ToContent [Entity School] where toContent = tc
instance ToTypedContent [Entity School] where toTypedContent = ttc

instance ToContent [Cost] where toContent = tc
instance ToTypedContent [Cost] where toTypedContent = ttc

instance ToContent PaymentIndecies where toContent = tc
instance ToTypedContent PaymentIndecies where toTypedContent = ttc

instance ToContent TagExpenseIndecies where toContent = tc
instance ToTypedContent TagExpenseIndecies where toTypedContent = ttc
