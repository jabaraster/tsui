{-# LANGUAGE PackageImports #-}
import "tsui" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
