'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setFilter = setFilter;
function setFilter(filter) {
  return { type: 'SET_FILTER', filter: filter };
}