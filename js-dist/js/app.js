'use strict';

var _redux = require('redux');

var _reducers = require('./reducers');

var _reducers2 = _interopRequireDefault(_reducers);

var _actions = require('./actions');

var actions = _interopRequireWildcard(_actions);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var initialState = {
    filter: {
        count: { label: '契約状況', filtered: false },
        percent: { label: '契約状況(%)', filtered: false },
        expense: { label: '支出', filtered: false },
        income: { label: '収入', filtered: false },
        balance: { label: '収支', filtered: false },
        cumulativeTotal: { label: '累積収支', filtered: false }
    }
};

var store = (0, _redux.createStore)(_reducers2.default, initialState);
store.subscribe(function () {
    console.log('---------------');
    console.log(store.getState());
});

store.dispatch(actions.setFilter({ item: 'count', filtered: true }));
store.dispatch(actions.setFilter({ item: 'percent', filtered: true }));