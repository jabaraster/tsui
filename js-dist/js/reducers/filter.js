'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var initialState = {
  count: { label: '契約状況', filtered: false },
  percent: { label: '契約状況(%)', filtered: false },
  expense: { label: '支出', filtered: false },
  income: { label: '収入', filtered: false },
  balance: { label: '収支', filtered: false },
  cumulativeTotal: { label: '累積収支', filtered: false }
};

function filter() {
  var state = arguments.length <= 0 || arguments[0] === undefined ? initialState : arguments[0];
  var action = arguments[1];

  switch (action.type) {
    case 'SET_FILTER':
      var targetItemValue = Object.assign({}, state[action.filter.item]);
      targetItemValue.filtered = action.filter.filtered;

      var targetItem = {};
      targetItem[action.filter.item] = targetItemValue;

      var newState = Object.assign({}, state, targetItem);
      return newState;

    default:
      return state;
  }
}

exports.default = filter;