
export function setFilter(filterItem) {
  return { type: 'SET_FILTER', filterItem }
}

export function switchFilterVisible() {
  return { type: 'SWITCH_FILTER_VISIBLE' }
}

export function initializePaymentBalances(paymentBalances) {
  return { type: 'INITIALIZE_PAYMENT_BALANCES', paymentBalances }
}

export function initialzeTagExpenses(tagExpenses) {
  return { type: 'INITIALIZE_TAG_EXPENSES', tagExpenses }
}

export function initializeCostParameters(costParameters) {
  return { type: 'INITIALIZE_COST_PARAMETERS', costParameters }
}
