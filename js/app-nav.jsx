'use strict';

import React    from 'react';
import ReactDOM from 'react-dom';

import Navbar      from 'react-bootstrap/lib/Navbar';
import Nav         from 'react-bootstrap/lib/Nav';
import NavItem     from 'react-bootstrap/lib/NavItem';
import NavDropdown from 'react-bootstrap/lib/NavDropdown';
import MenuItem    from 'react-bootstrap/lib/MenuItem';

import App from './lib/app.js';

const AppNav = React.createClass({
    handleNavItemClick(pKey1, pKey2) {
        switch (pKey1) {
        case 1:
            location.href = App.href('cost-parameters-edit-href');
            break;
        case 4:
            location.href = App.href('auth-logout-href');
            break;
        case 5:
            location.href = App.href('user-index-href');
            break;
        case 2.1:
            location.href = App.href('school-index-href');
            break;
        case 2.2:
            location.href = App.href('school-new-href');
            break;
        case 3.1:
            location.href = App.href('tag-order-index-href');
            break;
        case 3.2:
            location.href = App.href('tag-order-new-href');
            break;
        default:
            break; // nop
        }
    },
    render() {
        return (
          <div>
            <Navbar inverse fixedTop>
              <Navbar.Header>
                <Navbar.Brand>
                  <a href="/">ついしみゅ</a>
                </Navbar.Brand>
                <Navbar.Toggle />
              </Navbar.Header>
              {App.authenticated() ?
              <Navbar.Collapse>
                <Nav onSelect={this.handleNavItemClick}>
                  <NavItem eventKey={1} href="#">原価パラメータ編集</NavItem>
                  <NavDropdown eventKey={2} title="学校">
                    <MenuItem eventKey={2.1}>学校一覧</MenuItem>
                    <MenuItem eventKey={2.2}>新しい学校を追加</MenuItem>
                  </NavDropdown>
                  <NavDropdown eventKey={3} title="ICタグ">
                    <MenuItem eventKey={3.1}>ICタグ購入履歴</MenuItem>
                    <MenuItem eventKey={3.2}>ICタグ購入履歴を登録</MenuItem>
                  </NavDropdown>
                </Nav>
                <Nav pullRight onSelect={this.handleNavItemClick}>
                  <NavItem eventKey={5} href="#">ユーザー一覧</NavItem>
                  <NavItem eventKey={4} href="#">ログアウト</NavItem>
                </Nav>
              </Navbar.Collapse>
               : null
              }
            </Navbar>
          </div>
        );
    },
});
$(() => {
    ReactDOM.render(<AppNav />, document.getElementById('app-nav'));
});
