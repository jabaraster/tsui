
import { createStore } from 'redux'
import tsuiApp from './reducers'
import * as actions from './actions'

const initialState = {
    filter: {
        count:   { label: '契約状況', filtered: false },
        percent: { label: '契約状況(%)', filtered: false },
        expense: { label: '支出', filtered: false },
        income:  { label: '収入', filtered: false },
        balance: { label: '収支', filtered: false },
        cumulativeTotal: { label: '累積収支', filtered: false },
    }
}

const store = createStore(tsuiApp, initialState);
store.subscribe(() => {
    console.log('---------------')
    console.log(store.getState())
});

store.dispatch(actions.setFilter({ item: 'count', filtered: true }));
store.dispatch(actions.setFilter({ item: 'percent', filtered: true }));

