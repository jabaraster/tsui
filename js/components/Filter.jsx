
import React from 'react'
import Button         from 'react-bootstrap/lib/Button';
import Collapse       from 'react-bootstrap/lib/Collapse';
import Glyphicon      from 'react-bootstrap/lib/Glyphicon';
import Well           from 'react-bootstrap/lib/Well';

import { setFilter, switchFilterVisible } from '../actions'

const Filter = ({dispatch, filterState}) => {
    const onFilterVisibleChange= () => {
        dispatch(switchFilterVisible())
    }
    const onChange = (filterItem) => {
        dispatch(setFilter(Object.assign({}, filterItem, { filtered: !filterItem.filtered })))
    }
    return (
        <Well className="filter">
          <Button bsSize="small" onClick={onFilterVisibleChange}>
            <Glyphicon glyph="filter" />
          </Button>
          <Collapse in={filterState.visibility}>
            <div>
            {filterState.filterItems.map(filterItem => {
              return (
                <label className="filter" key={'filter_' + filterItem.name}>
                  <input type="checkbox"
                         checked={!filterItem.filtered}
                         onChange={() => { onChange(filterItem) }}
                  />
                  {filterItem.label}
                </label>
              );
            })}
            </div>
          </Collapse>
        </Well>
    )
}

export default Filter
