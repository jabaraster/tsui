
import React       from 'react'
import { connect } from 'react-redux'
import classnames  from 'classnames';
import App         from '../lib/app.js';

const Balance = ({data}) => {
    const balance = data.yearIncome.value - App.calcTotalCost(data)
    const cls = classnames({
                    positive: balance > 0,
                    negative: balance < 0
                })
    return (
        <div className="cell balance">
          <span className={cls}>
            {App.comma(balance)}
          </span>
        </div>
    )
}

Balance.propTypes = {
    data: React.PropTypes.object.isRequired
}

function mapStateToProps(state, props) {
  return props
}

export default connect(mapStateToProps)(Balance)
