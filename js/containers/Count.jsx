
import React       from 'react'
import { connect } from 'react-redux'

const Count = ({data}) => {
    const entry = data.entry;
    return (
        <div className="cell count">
          <span>{entry.studentCount}</span>
          /
          <span>{entry.tagAppliedCount}</span>
          /
          <span>{entry.tagChargedCount}</span>
        </div>
    )
}

Count.propTypes = {
    data: React.PropTypes.object.isRequired,
}

function mapStateToProps(state, props) {
  return props
}

export default connect(mapStateToProps)(Count)
