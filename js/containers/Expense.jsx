
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Popover        from 'react-bootstrap/lib/Popover';

import React       from 'react'
import { connect } from 'react-redux'
import App         from '../lib/app.js';

const Expense = ({data, lastPosition}) => {
    const tagPopover = (payment) => (
        <Popover title="内訳">
          <table className="cost-detail table">
            <tbody>
              {payment.cost.month.map((monthItem, i) => {
                 return ( <tr key={'month-cost_' + i}>
                            <th>{monthItem.label}(円×月数)</th>
                            <td>{App.comma(monthItem.value)}×{monthItem.monthCount}</td>
                          </tr> );
              })}
              {payment.cost.year.map((yearItem, i) => {
                 return ( <tr key={'year-cost_' + i}>
                            <th>{yearItem.label}</th>
                            <td>{App.comma(yearItem.value)}</td>
                          </tr> );
              })}
            </tbody>
          </table>
        </Popover>
        )
    return (
        <div className="cell expense">
          <OverlayTrigger placement={lastPosition?'left':'right'} overlay={tagPopover(data)}>
            <a className="popup-trigger">
              {App.comma(App.calcTotalCost(data))}
            </a>
          </OverlayTrigger>
        </div>
    )
}

Expense.propTypes = {
    data: React.PropTypes.object.isRequired,
    lastPosition: React.PropTypes.bool.isRequired,
}

function mapStateToProps(state, props) {
    return props
}

export default connect(mapStateToProps)(Expense)
