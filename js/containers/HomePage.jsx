
import Button         from 'react-bootstrap/lib/Button';
import Glyphicon      from 'react-bootstrap/lib/Glyphicon';
import Well           from 'react-bootstrap/lib/Well';
import Collapse       from 'react-bootstrap/lib/Collapse';

import Ajaxer from '../lib/ajaxer.js';
import App    from '../lib/app.js';

import * as actions   from '../actions'

import Total          from './Total'
import TagExpense     from './TagExpense'
import PaymentBalance from './PaymentBalance'
import VisibleFilter  from './VisibleFilter'

import React       from 'react'
import { connect } from 'react-redux'

const Page = ({dispatch, data, filterState}) => {
    const getTagExpenses = () => {
        if (data.paymentBalances.length === 0) return;

        const ajax = Ajaxer.get(App.href('tag-expense-index-href'));
        data.paymentBalances[0].payments.forEach((payment) => {
            ajax.query({ schoolYear: payment.schoolYear });
        });
        ajax.end((err, res) => {
            if (Ajaxer.evalError(err)) return;
            dispatch(actions.initialzeTagExpenses(res.body))
        });
    }
    const hasData = () => {
        if (data.paymentBalances.length === 0) return false;
        if (data.paymentBalances[0].payments.length === 0) return false;
        return true;
    }
    const getFirstPayments = () => {
        return data.paymentBalances[0].payments;
    }
    const onFilterClick = (filterName) => {
        var f = filterState[filterName];
        f.filtered = !f.filtered;
        dispatch(setFilter(filterState))
    }
    const switchFilterVisible = () => {
        dispatch(switchFilterVisible())
    }
    return (
        <div className="payment-balances">
          <Well className="filter">
            <Button bsSize="small" onClick={switchFilterVisible}>
              <Glyphicon glyph="filter" />
            </Button>
            <VisibleFilter />
          </Well>
          <div className="title">
            <div className="cell"><span>学校</span></div>
            <div className="cell"><span>年度</span></div>
            {!hasData() ? null :
               getFirstPayments().map((payment, i) => {
                   return ( <div key={'school-year_' + i}
                                className="cell school-year">
                              <span>{payment.schoolYear}</span>
                            </div> );
               })
            }
          </div>
          <div className="bs">
          {data.paymentBalances.map((paymentBalance, i) => {
              return ( <PaymentBalance key={'payment-balance_' + i}
                           data={paymentBalance} /> );
          })}
          </div>
          <div className="te">
          {data.tagExpenses.map((tagExpense, i) => {
              return ( <TagExpense key={'tag-expense_' + i}
                           data={tagExpense} /> );
          })}
          </div>
          <Total />
        </div>
    )
}

Page.propTypes = {
    data: React.PropTypes.object.isRequired,
    filterState: React.PropTypes.object.isRequired,
}

function mapStateToProps(state) {
    return state
}

export default connect(mapStateToProps)(Page)
