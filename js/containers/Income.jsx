
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Popover        from 'react-bootstrap/lib/Popover';

import React       from 'react'
import { connect } from 'react-redux'
import App         from '../lib/app.js';

const Income = ({data, lastPosition}) => {
    const tagPopover = (payment) => (
        <Popover title="詳細">
          <table className="income-description table">
            <tbody>
              {payment.yearIncome.description.map((desc, i) => {
                  return ( <tr key={'year-income-description_' + i}>
                             <th>{desc.label}</th>
                             <td className="description">{App.comma(desc.value)}</td>
                           </tr> );
              })}
            </tbody>
          </table>
        </Popover>
    )
    return (
        <div className="cell income">
          <OverlayTrigger placement={lastPosition ?'left':'right'} overlay={tagPopover(data)}>
            <a className="popup-trigger">
              {App.comma(data.yearIncome.value)}
            </a>
          </OverlayTrigger>
        </div>
    )
}

Income.propTypes = {
    data: React.PropTypes.object.isRequired,
    lastPosition: React.PropTypes.bool.isRequired,
}

function mapStateToProps(state, props) {
  return props
}

export default connect(mapStateToProps)(Income)
