
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger'
import Popover        from 'react-bootstrap/lib/Popover'

import Balance        from './Balance'
import Income         from './Income'
import Expense        from './Expense'
import Count          from './Count'
import Percent        from './Percent'
import Total          from './Total'
import TagExpense     from './TagExpense'

import App            from '../lib/app.js';

import React          from 'react'
import { connect }    from 'react-redux'

const PaymentBalance = ({paymentBalance, filterState}) => {
    const countPopover = (
            <Popover title="数字の見方">
              児童数/タグ保持数/有償オプション加入数
            </Popover>
          );
    const percentPopover = (
            <Popover title="数字の見方">
              タグ保持率/<br/>
              有償オプション加入率(対児童数)/<br/>
              有償オプション加入率(対タグ保持数)
            </Popover>
          );
    return (
        <div className="payment-balance">
          <div className="school cell">
            <a href={paymentBalance.schoolEditUrl}>
              {paymentBalance.school.name}
            </a>
          </div>
          <div className="values">
            {App.getFilterItem(filterState,'count').filtered ? null :
            <div className="counts">
              <div className="cell">
                <OverlayTrigger overlay={countPopover}>
                  <a href="#">契約状況</a>
                </OverlayTrigger>
              </div>
              {paymentBalance.payments.map((payment, i) => {
                   return ( <Count key={'count_' + i}
                                data={payment} /> );
              })}
            </div>
            }
            {App.getFilterItem(filterState,'percent').filtered ? null :
            <div className="percents">
              <div className="cell">
                <OverlayTrigger overlay={percentPopover}>
                  <a href="#">契約状況(%)</a>
                </OverlayTrigger>
              </div>
              {paymentBalance.payments.map((payment, i) => {
                   return ( <Percent key={'percent_' + i}
                                data={payment} /> );
              })}
            </div>
            }
            {App.getFilterItem(filterState,'expense').filtered ? null :
            <div className="expenses">
              <div className="cell">支出</div>
              {paymentBalance.payments.map((payment, i) => {
                   return ( <Expense key={'expense_' + i}
                                lastPosition={paymentBalance.payments.length===i+1}
                                data={payment} /> );
              })}
            </div>
            }
            {App.getFilterItem(filterState,'income').filtered ? null :
            <div className="incomes">
              <div className="cell">収入</div>
              {paymentBalance.payments.map((payment, i) => {
                   return ( <Income key={'income_' + i}
                                lastPosition={paymentBalance.payments.length===i+1}
                                data={payment} /> );
              })}
            </div>
            }
            {App.getFilterItem(filterState,'balance').filtered ? null :
            <div className="balances">
              <div className="cell">収支</div>
              {paymentBalance.payments.map((payment, i) => {
                   return ( <Balance key={'balance_' + i}
                                data={payment}
                            /> );
              })}
            </div>
            }
          </div>
        </div>
    )
}

PaymentBalance.propTypes = {
    paymentBalance: React.PropTypes.object.isRequired,
    filterState: React.PropTypes.object.isRequired,
}

function mapStateToProps(state, props) {
  return { paymentBalance: props.data, filterState: state.filterState }
}

export default connect(mapStateToProps)(PaymentBalance)
