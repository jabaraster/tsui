
import React       from 'react'
import { connect } from 'react-redux'
import App         from '../lib/app.js';

const Percent = ({data}) => {
    const entry = data.entry;
    const sc  = entry.studentCount;
    const tac = entry.tagAppliedCount;
    const tcc = entry.tagChargedCount;
    return (
        <div className="cell count">
          <span>{App.percent(tac, sc)}</span>
          /
          <span>{App.percent(tcc, sc)}</span>
          /
          <span>{App.percent(tcc, tac)}</span>
        </div>
    )
}

Percent.propTypes = {
    data: React.PropTypes.object.isRequired,
}

function mapStateToProps(state, props) {
  return props
}

export default connect(mapStateToProps)(Percent)
