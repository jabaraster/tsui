
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Popover        from 'react-bootstrap/lib/Popover';

import App         from '../lib/app.js';

import React       from 'react'
import { connect } from 'react-redux'

const TagExpense = ({data, costParameters}) => {
  const popover = (
      <Popover title="詳細">
        <table className="cost-detail table">
          <tbody>
            <tr>
              <th>リース開始</th>
              <td>{data.tagOrder.leaseStart.year}年{data.tagOrder.leaseStart.month}月</td>
            </tr>
            <tr>
              <th>リース料率</th>
              <td>
              {'leaseRatio' in costParameters ?
                costParameters.leaseRatio :
                '' }
              </td>
            </tr>
          </tbody>
        </table>
      </Popover>
  )
  const exs = data.costs.map(cost => {
    return (
      <div className="cell">
        {App.comma(cost.monthCost.monthCount * cost.monthCost.value)}
      </div>
    )
  })
  return (
      <div className="tag-expenses">
        <div className="tag-expense cell">
          <OverlayTrigger placement="right" overlay={popover}>
            <a className="popup-trigger" href={data.tagOrderEditUrl}>
              タグ{data.tagOrder.count}個
            </a>
          </OverlayTrigger>
        </div>
        <div className="cell">
          支出
        </div>
        {exs}
      </div>
  )
}
TagExpense.propTypes = {
    data: React.PropTypes.object.isRequired
}

function mapStateToProps(state, props) {
  console.log(state)
  const ret = Object.assign({}, props)
  ret.costParameters = state.costParameters
  return ret
}

export default connect(mapStateToProps)(TagExpense)
