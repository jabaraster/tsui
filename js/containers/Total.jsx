
import React       from 'react'
import { connect } from 'react-redux'
import classnames  from 'classnames';
import App         from '../lib/app.js';

const Total = ({data, filterState}) => {
    return (
        <div className="totals">
          <div className="total cell">総計</div>
          <div className="values">
          {App.getFilterItem(filterState,'expense').filtered ? null :
              <div className="expenses">
                <div className="cell">支出</div>
                {data.map((total,i) => {
                    return (
                        <div key={'total-expense_'+i} className="cell">
                          {App.comma(total.expense)}
                        </div>
                    );
                })}
              </div>
            }
            {App.getFilterItem(filterState,'income').filtered ? null :
              <div className="incomes">
                <div className="cell">収入</div>
                {data.map((total,i) => {
                    return (
                        <div key={'total-income'+i} className="cell">
                          {App.comma(total.income)}
                        </div>
                    );
                })}
              </div>
            }
            {App.getFilterItem(filterState,'balance').filtered ? null :
              <div className="balances">
                <div className="cell">収支</div>
                {data.map((total,i) => {
                    const balance = total.balance;
                    const cls = classnames({
                                    positive: balance > 0,
                                    negative: balance < 0,
                                });
                    return (
                        <div key={'total-balance_'+i} className="cell">
                          <span className={cls}>
                            {App.comma(balance)}
                          </span>
                        </div>
                    );
                })}
              </div>
            }
            {App.getFilterItem(filterState,'cumulativeTotal').filtered ? null :
              <div className="cumulative-totals">
                <div className="cell">累積収支</div>
                {data.map((total,i) => {
                    const ct = total.cumulativeTotal;
                    const cls = classnames({
                                    positive: ct > 0,
                                    negative: ct < 0,
                                });
                    return (
                        <div key={'cumulative-total'+i} className="cell">
                          <span className={cls}>
                            {App.comma(total.cumulativeTotal)}
                          </span>
                        </div>
                    );
                })}
              </div>
            }
          </div>
        </div>
    )
}

Total.propTypes = {
    data: React.PropTypes.array.isRequired,
    filterState: React.PropTypes.object.isRequired,
}

function mapStateToProps(state) {
  return { data: state.data.total, filterState: state.filterState }
}

export default connect(mapStateToProps)(Total)
