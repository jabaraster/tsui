
import Filter from '../components/Filter'
import { connect } from 'react-redux'

function mapStateToProps(state) {
    return { filterState: state.filterState }
}

const VisibleFilter = connect(mapStateToProps)(Filter)

export default VisibleFilter
