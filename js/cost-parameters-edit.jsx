'use strict';

import React    from 'react';
import ReactDOM from 'react-dom';

import Glyphicon     from 'react-bootstrap/lib/Glyphicon';
import Input         from 'react-bootstrap/lib/Input';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';


import Ajaxer from './lib/ajaxer.js';
import App    from './lib/app.js';
import Mixin  from './lib/mixin.js';

const Page = React.createClass({
    mixins: [Mixin],
    getInitialState() {
        return {
            params: {
                maintenance: 0,
                optionCharge: 0,
                aplusCharge: 0,
                newDocument: 0,
                updateDocument: 0,
                deliveryToSchool: 0,
                leaseRatio: 0,
                leaseYear: 0,
            }
        };
    },
    componentDidMount() {
        Ajaxer.get(App.href('cost-parameters-href'))
              .end((err, res) => {
                  if (Ajaxer.evalError(err)) return;
                  this.state.params = res.body;
                  this.setState({ });
              });
    },
    handleChange(pPropName) {
        const value = this.numberFromRef(pPropName);
        if (value > 0) {
            this.state.params[pPropName] = value;
            this.setState({});
        }
    },
    validateMaintenance() {
        return this.valid(this.state.params.maintenance);
    },
    validateOptionCharge() {
        return this.valid(this.state.params.optionCharge);
    },
    validateAplusCharge() {
        return this.valid(this.state.params.aplusCharge);
    },
    validateNewDocument() {
        return this.valid(this.state.params.newDocument);
    },
    validateUpdateDocument() {
        return this.valid(this.state.params.updateDocument);
    },
    validateDeliveryToSchool() {
        return this.valid(this.state.params.deliveryToSchool);
    },
    validateLeaseRatio() {
        return this.valid(this.state.params.leaseRatio);
    },
    validateLeaseYear() {
        return this.valid(this.state.params.leaseYear);
    },
    save() {
        Ajaxer.post(App.href('cost-parameters-href'))
              .send(this.state.params)
              .end((err, res) => {
                  if (Ajaxer.evalError(err)) return;
                  Ajaxer.evalJsonResponse(res);
              });
    },
    render() {
        return (
            <form>
              <Input type="number"
                min={0}
                value={this.state.params.maintenance}
                placeholder="システム保守費(月額)"
                label="システム保守費(月額)"
                bsStyle={this.validateMaintenance()}
                hasFeedback
                ref="maintenance"
                onChange={this.handleChange.bind(this, 'maintenance')}
              />
              <Input type="number"
                min={0}
                value={this.state.params.optionCharge}
                placeholder="有償オプション月額単価"
                label="有償オプション月額単価"
                bsStyle={this.validateOptionCharge()}
                hasFeedback
                ref="optionCharge"
                onChange={this.handleChange.bind(this, 'optionCharge')}
              />
              <Input type="number"
                min={0}
                value={this.state.params.aplusCharge}
                placeholder="アプラス手数料 (1件ごと)"
                label="アプラス手数料 (1件ごと)"
                bsStyle={this.validateAplusCharge()}
                hasFeedback
                ref="aplusCharge"
                onChange={this.handleChange.bind(this, 'aplusCharge')}
              />
              <Input type="number"
                min={0}
                value={this.state.params.newDocument}
                placeholder="新規児童資料"
                label="新規児童資料"
                bsStyle={this.validateNewDocument()}
                hasFeedback
                ref="newDocument"
                onChange={this.handleChange.bind(this, 'newDocument')}
              />
              <Input type="number"
                min={0}
                value={this.state.params.updateDocument}
                placeholder="既存児童の更新用資料"
                label="既存児童の更新用資料"
                bsStyle={this.validateUpdateDocument()}
                hasFeedback
                ref="updateDocument"
                onChange={this.handleChange.bind(this, 'updateDocument')}
              />
              <Input type="number"
                min={0}
                value={this.state.params.deliveryToSchool}
                placeholder="学校への郵送費"
                label="学校への郵送費"
                bsStyle={this.validateDeliveryToSchool()}
                hasFeedback
                ref="deliveryToSchool"
                onChange={this.handleChange.bind(this, 'deliveryToSchool')}
              />
              <Input type="number"
                min={0.01}
                value={this.state.params.leaseRatio}
                placeholder="リース料率"
                label="リース料率"
                bsStyle={this.validateLeaseRatio()}
                hasFeedback
                ref="leaseRatio"
                onChange={this.handleChange.bind(this, 'leaseRatio')}
              />
              <Input type="number"
                min={1}
                value={this.state.params.leaseYear}
                placeholder="リース年"
                label="リース年"
                bsStyle={this.validateLeaseYear()}
                hasFeedback
                ref="leaseYear"
                onChange={this.handleChange.bind(this, 'leaseYear')}
              />
              <ButtonToolbar>
                <button type="submit" className="btn btn-success btn-imp" onClick={this.save}>
                  <Glyphicon glyph="OK"/>
                  保存
                </button>
              </ButtonToolbar>
            </form>
        );
    },
});

$(() => {
    ReactDOM.render(<Page />, document.getElementById('page'));
});
