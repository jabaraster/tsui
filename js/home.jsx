
import React           from 'react'
import { connect }     from 'react-redux'
import { createStore } from 'redux'
import { Provider }    from 'react-redux'
import { render }      from 'react-dom'

import tsuiApp         from './reducers'
import Page            from './containers/HomePage'

import Ajaxer          from './lib/ajaxer.js';
import App             from './lib/app.js';
import * as actions    from './actions'

const initialState = {
    data: {
        paymentBalances: [],
        tagExpenses: [],
        total: [],
    },
    filterState: {
        visibility: false,
        filterItems: [
          { name: 'count'          , label: '契約状況'   , filtered: false },
          { name: 'percent'        , label: '契約状況(%)', filtered: false },
          { name: 'expense'        , label: '支出'       , filtered: false },
          { name: 'income'         , label: '収入'       , filtered: false },
          { name: 'balance'        , label: '収支'       , filtered: false },
          { name: 'cumulativeTotal', label: '累積収支'   , filtered: false },
        ]
    },
    costParameters: {}
}

const store = createStore(tsuiApp, initialState)

$(() => { // どーもcomponentDidMountが使えない？っぽいので$で代用. そんなわけないと思うのだが・・・
    Ajaxer.get(App.href('payment-balance-index-href')).end((err, res) => {
        if (Ajaxer.evalError(err)) return;

        const paymentBalances = res.body
        store.dispatch(actions.initializePaymentBalances(paymentBalances))

        if (!paymentBalances.length) {
            render(
                <Provider store={store}>
                  <Page />
                </Provider>,
                document.getElementById('page')
            )
            return;
        }

        const ajax = Ajaxer.get(App.href('tag-expense-index-href'));
        paymentBalances[0].payments.forEach((payment) => {
            ajax.query({ schoolYear: payment.schoolYear });
        });
        ajax.end((err, res) => {
            if (Ajaxer.evalError(err)) return;
            store.dispatch(actions.initialzeTagExpenses(res.body))

            render(
                <Provider store={store}>
                  <Page />
                </Provider>,
                document.getElementById('page')
            )
        });

        Ajaxer.get(App.href('cost-parameters-href')).
          end((err, res) => {
            store.dispatch(actions.initializeCostParameters(res.body))
          });
    })
})
