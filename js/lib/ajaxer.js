'use strict';

import request from 'superagent';

module.exports = {
    post(pUrl) {
        return request.post(pUrl)
               .set(csrfHeaderName, csrfToken)
               .type('json')
               .accept('json')
               ;
    },
    get(pUrl) {
        return request.get(pUrl)
               .set(csrfHeaderName, csrfToken)
               .type('json')
               .accept('json')
               ;
    },
    put(pUrl) {
        return request.put(pUrl)
               .set(csrfHeaderName, csrfToken)
               .type('json')
               .accept('json')
               ;
    },
    del(pUrl) {
        return request.del(pUrl)
               .set(csrfHeaderName, csrfToken)
               .type('json')
               .accept('json')
               ;
    },
    evalJsonResponse(pSuperAgentResponse) {
        const body = pSuperAgentResponse.body;
        if (body.tag === 'DialogRedirect' && body.dialogMessage) {
            swal({
                title: '',
                text: body.dialogMessage,
                type: 'success',
            }, () => {
                location.href = body.redirectPath;
            });
            return;
        }
        if (body.tag === 'SuccessDialog' && body.dialogMessage) {
            swal({
                title: '',
                text: body.dialogMessage,
                showConfirmButton: false,
                type: 'success',
                timer: 1100,
            });
            return;
        }
        if (body.tag === 'ErrorDialog' && body.dialogMessage) {
            swal({
                title: '',
                text: body.dialogMessage,
                type: 'error',
            });
            return;
        }
        if (body.redirectPath) {
            location.href = body.redirectPath;
            return;
        }
    },
    evalError(pError) {
        if (pError) {
            swal({
                title: '通信エラー',
                text: ['status: ', pError.status, '<br/>', JSON.stringify(pError.response.body)].join(''),
                html: true,
                type: 'error',
            });
            return true;
        }
        return false;
    },
};
