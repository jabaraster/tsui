'use strict';

export default {
    getFilterItem(filterState, itemName) {
        const ary = filterState.filterItems.filter((filterItem) => {
            return itemName === filterItem.name
        });
        return ary.length === 1 ? ary[0] : null
    },
    showLoadingIcon() {
        $('#global-initial-loader').fadeIn();
    },
    hideLoadingIcon() {
        $('#global-initial-loader').fadeOut();
    },
    href(pTagId) {
        return $('#' + pTagId).attr('href');
    },
    text(pTagId) {
        return $('#' + pTagId).text();
    },
    authenticated() {
        return this.text('authenticated') === 'True'
    },
    doubleConfirm(pFirstText, pSecondText, pOkOperation) {
        swal({
            title: '',
            text: pFirstText,
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'はい',
            confirmButtonText: 'いいえ',
            confirmButtonColor: 'rgb(193,193,193)',
            closeOnCancel: false,
            closeOnConfirm: true,
        }, (isConfirm) => {
            if (isConfirm) return;
            // cancelとOKを反転させる. ボタン連打に対処するため.
            swal({
                title: '',
                text: pSecondText,
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'いいえ',
                confirmButtonText: 'はい',
                confirmButtonColor: 'rgb(200,143,143)',
                closeOnCancel: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, (isConfirm) => {
                if (!isConfirm) return;
                pOkOperation();
            });
        });
    },
    comma(pNumber) {
        return String(pNumber).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    },
    calcTotalCost(pPayment) {
        const cost = pPayment.cost;
        const monthCostTotal = cost.month.reduce((acc, item) => {
            return acc + item.value * item.monthCount;
        }, 0);
        const yearCostTotal = cost.year.reduce((acc, item) => {
            return acc + item.value;
        }, 0);
        return monthCostTotal + yearCostTotal;
    },
    percent(pNumerator, pDenominator) {
        if (pDenominator === 0) return '-';
        const v = pNumerator * 100 / pDenominator;
        const s = (Math.round(v * 10) / 10) + '';
        return s.indexOf('.') < 0 ? s + '.0' : s ;
    },
    total(pPaymentBalances) {
        if (pPaymentBalances.length === 0) return [];
        // 年度ごとの収支配列を作る
        const ret = pPaymentBalances[0].payments.map(_ => {
            return { expense: 0, income: 0, balance: 0, cumulativeTotal: 0, };
        });
        pPaymentBalances.forEach(paymentBalance => {
            paymentBalance.payments.forEach((payment, idx) => {
                const ex = this.calcTotalCost(payment);
                const e = ret[idx];
                e.expense = e.expense + ex;
                e.income  = e.income  + payment.yearIncome.value;
                e.balance = e.balance + (e.income - e.expense);
                if (idx === 0) {
                    e.cumulativeTotal = e.balance;
                } else {
                    e.cumulativeTotal = e.balance + ret[idx-1].cumulativeTotal;
                }
            });
        });
        return ret;
    },
};
