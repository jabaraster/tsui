'use strict';

import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';

module.exports = React.createClass({
    propTypes: {
        show: React.PropTypes.bool.isRequired,
        title: React.PropTypes.string.isRequired,
        onCancel: React.PropTypes.func.isRequired,
        onOk: React.PropTypes.func.isRequired,
    },
    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.onCancel}>
              <Modal.Header closeButton>
                <Modal.Title>{this.props.title}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                {this.props.children}
              </Modal.Body>
              <Modal.Footer>
                <Button onClick={this.props.onCancel}>キャンセル</Button>
                <Button onClick={this.props.onOk} bsStyle="primary">OK</Button>
              </Modal.Footer>
            </Modal>
        );
    },
});
