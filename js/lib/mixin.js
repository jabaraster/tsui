'use strict';

module.exports = {
    isValidMonth(pMonth) {
        return 1 <= pMonth && pMonth <= 12 ;
    },
    isValidYear(pYear) {
        return pYear >= this.minYear;
    },
    numberFromRef(pRefName) {
        return this.refs[pRefName].getValue() - 0;
    },
    booleanFromRef(pRefName) {
        return this.refs[pRefName].getValue() === 'on';
    },
    valid(pValidation) {
        return pValidation ? 'success' : 'error' ;
    },
    labelClassName: 'col-xs-2',
    wrapperClassName: 'col-xs-10',
    minYear: 2015,
};
