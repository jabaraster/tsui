'use strict';

module.exports = {
    schoolYearCol: 1,
    studentCountCol: 2,
    tagChargedCountCol: 2,
    numberCol: 2,
};
