
function costParameters(state={}, action) {
  console.log(action)
  switch (action.type) {
    case 'INITIALIZE_COST_PARAMETERS':
      return action.costParameters
    default:
      return state
  }
}

export default costParameters
