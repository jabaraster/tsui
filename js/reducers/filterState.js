
function filterState(state={}, action) {
  switch(action.type) {
    case 'SET_FILTER':
      return { visibility: state.visibility,
               filterItems: state.filterItems.map((filterItem) => {
                   return filterItem.name === action.filterItem.name
                          ? action.filterItem
                          : filterItem
               })
             }

    case 'SWITCH_FILTER_VISIBLE':
      return Object.assign({}, state, { visibility: !state.visibility })

    default:
      return state
  }
}

export default filterState
