
import { combineReducers } from 'redux'
import data                from './paymentBalances'
import filterState         from './filterState'
import costParameters      from './costParameters'

const tsuiApp = combineReducers({ data, filterState, costParameters })

export default tsuiApp
