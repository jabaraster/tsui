
import App from '../lib/app.js'

function paymentBalances(state={}, action) {
    switch (action.type) {
    case 'INITIALIZE_PAYMENT_BALANCES': {
        const paymentBalances = action.paymentBalances
        const total = App.total(paymentBalances)
        return { paymentBalances, tagExpenses: [], total }
    }
    case 'INITIALIZE_TAG_EXPENSES': {
        const paymentBalances = state.paymentBalances
        const total = App.total(paymentBalances)
        const tagExpenses = action.tagExpenses
        return { paymentBalances, tagExpenses, total }
    }
    default:
        return state;
    }
}

export default paymentBalances
