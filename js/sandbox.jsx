
import React from 'react'
import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import tsuiApp from './reducers'
import VisibleFilter from './containers/VisibleFilter'

const initialState = {
    filterState: {
        visibility: true,
        filterItems: [
            { name: 'count'          , label: '契約状況'   , filtered: false },
            { name: 'percent'        , label: '契約状況(%)', filtered: false },
            { name: 'expense'        , label: '支出'       , filtered: false },
            { name: 'income'         , label: '収入'       , filtered: false },
            { name: 'balance'        , label: '収支'       , filtered: false },
            { name: 'cumulativeTotal', label: '累積収支'   , filtered: false }
        ]
    }
}

const store = createStore(tsuiApp, initialState)
render(
  <Provider store={store}>
    <VisibleFilter />
  </Provider>,
  document.getElementById('page')
)
