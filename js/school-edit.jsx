'use strict';

import React    from 'react';
import ReactDOM from 'react-dom';

import CSSTransitionGroup from 'react-addons-css-transition-group';

import Input          from 'react-bootstrap/lib/Input';
import Row            from 'react-bootstrap/lib/Row';
import Col            from 'react-bootstrap/lib/Col';
import Glyphicon      from 'react-bootstrap/lib/Glyphicon';
import Button         from 'react-bootstrap/lib/Button';
import ButtonToolbar  from 'react-bootstrap/lib/ButtonToolbar';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem       from 'react-bootstrap/lib/MenuItem';
import Modal          from 'react-bootstrap/lib/Modal';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Popover        from 'react-bootstrap/lib/Popover';
import SplitButton    from 'react-bootstrap/lib/SplitButton';
import Well           from 'react-bootstrap/lib/Well';

import Ajaxer          from './lib/ajaxer.js';
import App             from './lib/app.js';
import Mixin           from './lib/mixin.js';
import SchoolEditMixin from './lib/school-edit-mixin.js';

import classnames from 'classnames';

const TsuiEntryInputs = React.createClass({
    mixins: [Mixin, SchoolEditMixin],
    propTypes: {
        data: React.PropTypes.object.isRequired,
        onChange: React.PropTypes.func.isRequired,
        onAddEntry: React.PropTypes.func.isRequired,
        onRemoveEntry: React.PropTypes.func.isRequired,
    },
    handleNumberValueChange(pRefName) {
        this.props.data[pRefName] = this.numberFromRef(pRefName);
        this.props.onChange(this.props.data);
    },
    validateSchoolYear() {
        return this.validateNumber('schoolYear', this.minYear);
    },
    validateStudentCount() {
        return this.validateNumber('studentCount', 0);
    },
    validateTagAppliedCount() {
        return this.validateNumber('tagAppliedCount', 0);
    },
    validateTagChargedCount() {
        return this.validateNumber('tagChargedCount', 0);
    },
    validateTagNotAppliedCount() {
        return this.validateNumber('tagNotAppliedCount', 0);
    },
    validateNumber(pPropertyName, pMinValue) {
        return this.valid(this.props.data[pPropertyName] >= pMinValue);
    },
    onRowMenuSelected(id) {
        switch (id) {
            case 'add':
                this.addEntry();
                break;
            case 'remove':
                this.removeEntry();
                break;
            default:
                break;
        }
    },
    addEntry() {
        this.props.onAddEntry();
    },
    removeEntry() {
        this.props.onRemoveEntry();
    },
    calcPercent(pPropertyName) {
        const studentCount = this.props.data.studentCount;
        if (studentCount === 0) return '0%';
        const percent = 100 * this.props.data[pPropertyName] / studentCount;
        return  Math.floor(percent * 100) / 100 + '%';
    },
    render() {
        const wrapperClassName = 'col-xs-12';
        const entry = this.props.data;
        const wrongTotal = entry.studentCount !== entry.tagAppliedCount + entry.tagNotAppliedCount;
        const cls = classnames({ 'entry': true,
                                 'has-warning':  wrongTotal,
                               })
        return (
            <Row className={cls}>
              <Col xs={this.schoolYearCol} className="school-year">
                {wrongTotal ?
                  ( <span className="alert-warning">
                      <OverlayTrigger overlay={<Popover>タグ申込数とタグ不要数の合計が、生徒数と一致しません。入力ミスの可能性はありませんか？</Popover>}>
                        <Glyphicon glyph="exclamation-sign" />
                      </OverlayTrigger>
                    </span> ) :
                  ( <span className="alert-success">
                      <Glyphicon glyph="ok-sign" />
                    </span> )
                }
                <span className="school-year">{entry.schoolYear}</span>
                <DropdownButton bsSize="xsmall"
                                onSelect={this.onRowMenuSelected}
                >
                  <MenuItem eventKey="add">
                    <Glyphicon glyph="plus"  /> 下にコピーを追加
                  </MenuItem>
                  <MenuItem eventKey="remove">
                    <Glyphicon glyph="trash" /> 削除
                  </MenuItem>
                </DropdownButton>
              </Col>
              <Col xs={this.studentCountCol}>
                <Input type="number"
                       value={entry.studentCount}
                       min={0}
                       placeholder="生徒数"
                       bsStyle={this.validateStudentCount()}
                       ref="studentCount"
                       wrapperClassName={wrapperClassName}
                       onChange={this.handleNumberValueChange.bind(this, 'studentCount')}
                />
              </Col>
              <Col xs={this.tagChargedCountCol}>
                <Input type="number"
                       value={entry.tagAppliedCount}
                       min={0}
                       placeholder="タグ申込数"
                       bsStyle={this.validateTagAppliedCount()}
                       help={this.calcPercent('tagAppliedCount')}
                       ref="tagAppliedCount"
                       wrapperClassName={wrapperClassName}
                       onChange={this.handleNumberValueChange.bind(this, 'tagAppliedCount')}
                />
              </Col>
              <Col xs={this.numberCol}>
                <Input type="number"
                       value={entry.tagChargedCount}
                       min={0}
                       placeholder="有償オプション加入数"
                       bsStyle={this.validateTagChargedCount()}
                       help={this.calcPercent('tagChargedCount')}
                       ref="tagChargedCount"
                       wrapperClassName={wrapperClassName}
                       onChange={this.handleNumberValueChange.bind(this, 'tagChargedCount')}
                />
              </Col>
              <Col xs={this.numberCol}>
                <Input type="number"
                       value={entry.tagNotAppliedCount}
                       min={0}
                       placeholder="タグ不要人数"
                       bsStyle={this.validateTagNotAppliedCount()}
                       help={this.calcPercent('tagNotAppliedCount')}
                       ref="tagNotAppliedCount"
                       wrapperClassName={wrapperClassName}
                       onChange={this.handleNumberValueChange.bind(this, 'tagNotAppliedCount')}
                />
              </Col>
            </Row>
        );
    },
});

const Page = React.createClass({
    mixins: [Mixin, SchoolEditMixin],
    getInitialState() {
        const minYear = this.minYear;
        const entries = [this.newEntry(minYear)];
        return {
            school: {
                name: '',
                descriptor: 'Public',
                gateCount: 0,
            },
            installation: {
                schoolId: -1,
                serviceStart: {
                    year: minYear, month: 12
                },
                leaseStart: {
                    year: minYear, month: 12
                },
                chargedStart: {
                    year: minYear, month: 12
                },
                constructionExpense: 0,
            },
            entries: entries,
            showModal: false,
            percentStep: 5,
            maxChargedPercent: 75,
        };
    },
    newEntry(pYear) {
        return {
            schoolId: -1,
            schoolYear: pYear,
            studentCount: 0,
            tagAppliedCount: 0,
            tagChargedCount: 0,
            tagNotAppliedCount: 0,
        };
    },
    isNewSchoolMode() {
        return location.href.indexOf(App.href('school-new-href')) >= 0;
    },
    componentDidMount() {
        if (this.isNewSchoolMode()) {
            // nop. 新規登録画面に付き
            return;
        }
        // 編集画面の場合、学校情報をサーバから取得する.
        Ajaxer.get(App.href('school-href'))
              .end((err, res, status) => {
                  if (Ajaxer.evalError(err)) return;
                  const body = res.body;
                  this.setState({ school: body.school,
                                  installation: body.installation,
                                  entries: body.entries
                  });
              });
    },
    validateSchoolName() {
        return this.valid(this.state.school.name.length);
    },
    validateServiceStartYear() {
        return this.valid(this.isValidYear(this.state.installation.serviceStart.year));
    },
    validateServiceStartMonth() {
        return this.valid(this.isValidMonth(this.state.installation.serviceStart.month));
    },
    validateLeaseStartYear() {
        return this.valid(this.isValidYear(this.state.installation.leaseStart.year));
    },
    validateLeaseStartMonth() {
        return this.valid(this.isValidMonth(this.state.installation.leaseStart.month));
    },
    validateChargedStartYear() {
        return this.valid(this.isValidYear(this.state.installation.chargedStart.year));
    },
    validateChargedStartMonth() {
        return this.valid(this.isValidMonth(this.state.installation.chargedStart.month));
    },
    validateGateCount() {
        return this.valid(this.state.school.gateCount >= 0);
    },
    validateConstrunctionExpense() {
        return this.valid(this.state.installation.constructionExpense >= 0);
    },
    handleSchoolNameChange() {
        const name = this.refs.schoolName.getValue();
        this.state.school.name = name;
        this.setState({ school: this.state.school });
    },
    handleGateCountChange() {
        const gateCount = this.numberFromRef('gateCount');
        if (gateCount >= 0) {
            this.state.school.gateCount = gateCount;
            this.setState({ school: this.state.school });
        }
    },
    handleConstrunctionExpenseChange() {
        const constructionExpense = this.numberFromRef('constructionExpense');
        if (constructionExpense >= 0) {
            this.state.installation.constructionExpense = constructionExpense;
            this.setState({ installation: this.state.installation });
        }
    },
    handleServiceStartYearChange() {
        const year = this.numberFromRef('serviceStartYear');
        if (this.isValidYear(year)) {
            this.state.installation.serviceStart.year = year;
            this.reorderYear();
            this.setState({ installation: this.state.installation });
        }
    },
    handleServiceStartMonthChange() {
        const month = this.numberFromRef('serviceStartMonth');
        if (this.isValidMonth(month)) {
            this.state.installation.serviceStart.month = month;
            this.setState({ installation: this.state.installation });
        }
    },
    handleLeaseStartYearChange() {
        const year = this.numberFromRef('leaseStartYear');
        if (this.isValidYear(year)) {
            this.state.installation.leaseStart.year = year;
            this.reorderYear();
            this.setState({ installation: this.state.installation });
        }
    },
    handleLeaseStartMonthChange() {
        const month = this.numberFromRef('leaseStartMonth');
        if (this.isValidMonth(month)) {
            this.state.installation.leaseStart.month = month;
            this.setState({ installation: this.state.installation });
        }
    },
    handleChargedStartYearChange() {
        const year = this.numberFromRef('chargedStartYear');
        if (this.isValidYear(year)) {
            this.state.installation.chargedStart.year = year;
            this.setState({ installation: this.state.installation });
        }
    },
    handleChargedStartMonthChange() {
        const month = this.numberFromRef('chargedStartMonth');
        if (this.isValidMonth(month)) {
            this.state.installation.chargedStart.month = month;
            this.setState({ installation: this.state.installation });
        }
    },
    handlePublicChange() {
        const descPublic = this.booleanFromRef('descPublic');
        this.state.school.descriptor = descPublic ? 'Public' : 'Private' ;
        this.setState({ school: this.state.school });
    },
    handlePrivateChange() {
        const descPrivate = this.booleanFromRef('descPrivate');
        this.state.school.descriptor = !descPrivate ? 'Public' : 'Private' ;
        this.setState({ school: this.state.school });
    },
    handleEntryValueChange() {
        this.setState({ entries: this.state.entries });
    },
    reorderYear() {
        this.state.entries.forEach((pEntry, pIndex) => {
            pEntry.schoolYear = this.state.installation.leaseStart.year + pIndex;
        });
        this.setState({ entries: this.state.entries });
    },
    handleAddEntry(pIndex) {
        const entries = this.state.entries;
        const entry = entries[pIndex];
        const newEntry = JSON.parse(JSON.stringify(entry));
        newEntry.schoolYear = entry.schoolYear + 1;
        entries.splice(pIndex + 1, 0, newEntry);
        this.reorderYear();
    },
    handleRemoveEntry(pIndex) {
        if (this.state.entries.length === 1) {
            return;
        }
        this.state.entries.splice(pIndex, 1);
        this.reorderYear();
    },
    handleControlButtonClick(pMenuItemKey) {
      console.log(arguments[0])
      console.log(arguments[1])
        switch (pMenuItemKey) {
          case 1.1:
            this.setState({ showModal: true });
            break;
          default:
            break;
        }
    },
    handleNumberValueChanged(pRefName) {
        const val = this.numberFromRef(pRefName);
        const newState = {};
        newState[pRefName] = val;
        this.setState(newState);
    },
    applyPercentStep() {
        const step = this.numberFromRef('percentStep');
        const maxPercent  = this.numberFromRef('maxChargedPercent');
        this.state.entries.forEach((entry, idx, ary) => {
            if (idx === 0) return;
            const pre = ary[idx - 1];
            const prePercent = pre.tagChargedCount * 100 / pre.studentCount;
            const curPercent = prePercent + step <= maxPercent ?
                                 prePercent + step :
                                 maxPercent ;
            entry.tagChargedCount = Math.round(entry.studentCount * curPercent / 100);
        });
        this.setState({ entries: this.state.entries }, () => {
            this.closeModal();
        });
    },
    closeModal() {
        this.setState({ showModal: false });
    },
    save() {
        const ajax = this.isNewSchoolMode() ?
                      Ajaxer.put(App.href('school-index-href')) :
                      Ajaxer.post(App.href('school-href')) ;
        ajax.send(this.state)
            .end((err, res) => {
                if (Ajaxer.evalError(err)) return;
                Ajaxer.evalJsonResponse(res);
            });
    },
    render() {
        const entryInputs = this.state.entries.map((pEntry, pIndex) => {
            return (
                <TsuiEntryInputs key={'TsuiEntryInputs_' + pIndex}
                  data={pEntry}
                  onChange={this.handleEntryValueChange}
                  onAddEntry={this.handleAddEntry.bind(this, pIndex)}
                  onRemoveEntry={this.handleRemoveEntry.bind(this, pIndex)}
                />
            );
        });
        const header = (
            <Row className="header">
              <Col xs={this.schoolYearCol} className="school-year">
                <label>年度</label>
              </Col>
              <Col xs={this.studentCountCol}>
                <label>生徒数</label>
              </Col>
              <Col xs={this.tagChargedCountCol}>
                <label>タグ申込数</label>
              </Col>
              <Col xs={this.numberCol}>
                <label>有償オプション数</label>
              </Col>
              <Col xs={this.numberCol}>
                <label>タグ不要数</label>
              </Col>
            </Row>
        );
        return (
            <div className="page">
              <form className="form-horizontal entry-form">
                <Input type="text"
                  value={this.state.school.name}
                  placeholder="学校名"
                  label="学校名"
                  help="必ず入力してください。他の学校と重複してはいけません。"
                  bsStyle={this.validateSchoolName()}
                  hasFeedback
                  ref="schoolName"
                  labelClassName={this.labelClassName}
                  wrapperClassName={this.wrapperClassName}
                  onChange={this.handleSchoolNameChange}
                />
                <Input wrapperClassName="col-xs-offset-2 wrapper">
                  <Col xs={2}>
                    <Input type="radio"
                      checked={this.state.school.descriptor === 'Public'}
                      label="公立"
                      hasFeedback
                      ref="descPublic"
                      wrapperClassName={'col-xs-offset-2 ' + this.wrapperClassName}
                      onChange={this.handlePublicChange}
                    />
                  </Col>
                  <Col xs={2}>
                    <Input type="radio"
                           checked={this.state.school.descriptor === 'Private'}
                           label="私立"
                           hasFeedback
                           ref="descPrivate"
                           wrapperClassName={'col-xs-offset-2 ' + this.wrapperClassName}
                           onChange={this.handlePrivateChange}
                    />
                  </Col>
                </Input>
                <Input label="運用開始年月"
                       wrapperClassName="col-xs-offset-2 wrapper"
                       labelClassName={this.labelClassName}
                >
                  <Row>
                    <Col xs={3}>
                      <Input type="number"
                        value={this.state.installation.serviceStart.year}
                        min={this.minYear}
                        placeholder="年"
                        bsStyle={this.validateServiceStartYear()}
                        hasFeedback
                        ref="serviceStartYear"
                        labelClassName={this.labelClassName}
                        wrapperClassName={this.wrapperClassName}
                        onChange={this.handleServiceStartYearChange}
                      />
                    </Col>
                    <Col xs={3}>
                      <Input type="number"
                        value={this.state.installation.serviceStart.month}
                        min={1} max={12}
                        placeholder="月"
                        bsStyle={this.validateServiceStartMonth()}
                        hasFeedback
                        ref="serviceStartMonth"
                        labelClassName={this.labelClassName}
                        wrapperClassName={this.wrapperClassName}
                        onChange={this.handleServiceStartMonthChange}
                      />
                    </Col>
                  </Row>
                </Input>
                <Input label="リース開始年月"
                       wrapperClassName="col-xs-offset-2 wrapper"
                       labelClassName={this.labelClassName}
                >
                  <Row>
                    <Col xs={3}>
                      <Input type="number"
                        value={this.state.installation.leaseStart.year}
                        min={this.minYear}
                        placeholder="年"
                        bsStyle={this.validateLeaseStartYear()}
                        hasFeedback
                        ref="leaseStartYear"
                        labelClassName={this.labelClassName}
                        wrapperClassName={this.wrapperClassName}
                        onChange={this.handleLeaseStartYearChange}
                      />
                    </Col>
                    <Col xs={3}>
                      <Input type="number"
                        value={this.state.installation.leaseStart.month}
                        min={1} max={12}
                        placeholder="月"
                        bsStyle={this.validateLeaseStartMonth()}
                        hasFeedback
                        ref="leaseStartMonth"
                        labelClassName={this.labelClassName}
                        wrapperClassName={this.wrapperClassName}
                        onChange={this.handleLeaseStartMonthChange}
                      />
                    </Col>
                  </Row>
                </Input>
                <Input label="有料オプション開始年月"
                       wrapperClassName="col-xs-offset-2 wrapper"
                       labelClassName={this.labelClassName}
                >
                  <Row>
                    <Col xs={3}>
                      <Input type="number"
                        value={this.state.installation.chargedStart.year}
                        min={this.minYear}
                        placeholder="年"
                        bsStyle={this.validateChargedStartYear()}
                        hasFeedback
                        ref="chargedStartYear"
                        labelClassName={this.labelClassName}
                        wrapperClassName={this.wrapperClassName}
                        onChange={this.handleChargedStartYearChange}
                      />
                    </Col>
                    <Col xs={3}>
                      <Input type="number"
                        value={this.state.installation.chargedStart.month}
                        min={1} max={12}
                        placeholder="月"
                        bsStyle={this.validateChargedStartMonth()}
                        hasFeedback
                        ref="chargedStartMonth"
                        labelClassName={this.labelClassName}
                        wrapperClassName={this.wrapperClassName}
                        onChange={this.handleChargedStartMonthChange}
                      />
                    </Col>
                  </Row>
                </Input>
                <Input wrapperClassName="col-xs-offset-2 wrapper"
                       labelClassName={this.labelClassName}
                >
                  <Row>
                    <Col xs={4}>
                      <Input type="number"
                        value={this.state.school.gateCount}
                        min={0}
                        label="門の数"
                        placeholder="門の数"
                        bsStyle={this.validateGateCount()}
                        hasFeedback
                        ref="gateCount"
                        onChange={this.handleGateCountChange}
                      />
                    </Col>
                    <Col xs={4}>
                      <Input type="number"
                        value={this.state.installation.constructionExpense}
                        min={0}
                        label="工事費"
                        placeholder="工事費"
                        bsStyle={this.validateConstrunctionExpense()}
                        hasFeedback
                        ref="constructionExpense"
                        onChange={this.handleConstrunctionExpenseChange}
                      />
                    </Col>
                  </Row>
                </Input>

                <h4 className="entries-title">年度毎申込状況</h4>

                <ButtonToolbar>
                  <Button className="btn-imp"
                          bsStyle="success"
                          onClick={this.save}
                  >
                    <Glyphicon glyph="ok" />
                    保存
                  </Button>
                </ButtonToolbar>

                <Well>
                  <ButtonToolbar>
                    <SplitButton title="加入率を操作" onSelect={this.handleControlButtonClick}>
                      <MenuItem eventKey={1.1}>一定の％で上げていく</MenuItem>
                    </SplitButton>
                  </ButtonToolbar>
                </Well>

                {header}
                <div className="entries">
                  {entryInputs}
                </div>

                <ButtonToolbar>
                  <Button className="btn-imp"
                          bsStyle="success"
                          onClick={this.save}
                  >
                    <Glyphicon glyph="ok"/>
                    保存
                  </Button>
                  <a className="btn btn-default btn-imp" href={App.href('school-index-href')}>
                    <Glyphicon glyph="arrow-left"/>
                    キャンセル
                  </a>
                </ButtonToolbar>
              </form>

              <Modal show={this.state.showModal} onHide={this.closeModal}>
                <Modal.Header closeButton>
                  <Modal.Title>一定の％で上げていく</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <p>
                  最初の年度の有償オプション加入率を基準に、毎年度一定の％で加入率が上がるように有償オプション加入数を変更します。
                  </p>
                  <Input type="number"
                    min={1}
                    max={100}
                    value={this.state.percentStep}
                    label="％"
                    placeholder=""
                    hasFeedback
                    ref="percentStep"
                    onChange={this.handleNumberValueChanged.bind(this, 'percentStep')}
                  />
                  <Input type="number"
                    min={1}
                    max={100}
                    value={this.state.maxChargedPercent}
                    label="最大加入率（％）"
                    placeholder=""
                    hasFeedback
                    ref="maxChargedPercent"
                    onChange={this.handleNumberValueChanged.bind(this, 'maxChargedPercent')}
                  />
                </Modal.Body>
                <Modal.Footer>
                  <Button onClick={this.closeModal}>キャンセル</Button>
                  <Button onClick={this.applyPercentStep} bsStyle="primary">適用</Button>
                </Modal.Footer>
              </Modal>
            </div>
        );
    },
});

$(() => {
    ReactDOM.render(<Page />, document.getElementById('page'));
});
