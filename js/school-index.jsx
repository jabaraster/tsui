'use strict';

import Ajaxer from './lib/ajaxer.js';
import App    from './lib/app.js';

$(() => {
    $('.delete-button').click(function() {
        App.doubleConfirm(
            '学校情報を削除しますが、よろしいですか？関連する情報も全て削除されます。',
            'この操作は取り消せません。本当に削除しますか？',
            () => {
                Ajaxer.del($(this).find('a.hidden').attr('href'))
                    .end((err, res) => {
                        if (Ajaxer.evalError(err)) return;
                        Ajaxer.evalJsonResponse(res);
                    });
            }
        );
    });
});
