'use strict';

import React    from 'react';
import ReactDOM from 'react-dom';

import Glyphicon     from 'react-bootstrap/lib/Glyphicon';
import Input         from 'react-bootstrap/lib/Input';
import Row           from 'react-bootstrap/lib/Row';
import Col           from 'react-bootstrap/lib/Col';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';

import Ajaxer from './lib/ajaxer.js';
import App    from './lib/app.js';
import Mixin  from './lib/mixin.js';

const Page = React.createClass({
    mixins: [Mixin],
    getInitialState() {
        return {
            data: {
                leaseStart: {
                    year: 2015, month: 12
                },
                count: 1000,
                totalPrice: 1000 * 2000,
            },
            tagPrice: 0,
        };
    },
    isNewTagOrderMode() {
        return location.href.indexOf(App.href('tag-order-new-href')) >= 0;
    },
    componentDidMount() {
        Ajaxer.get(App.href('cost-parameters-href'))
              .end((err, res) => {
                  if (Ajaxer.evalError(err)) return;
                  this.setState({ tagPrice: res.body.tagPrice });
              });
        if (this.isNewTagOrderMode()) {
            return;
        }
        Ajaxer.get(App.href('tag-order-href'))
              .end((err, res) => {
                  if (Ajaxer.evalError(err)) return;
                  const body = res.body;
                  this.setState({ data: body });
              });
    },
    save() {
        const ajax = this.isNewTagOrderMode() ?
                      Ajaxer.put(App.href('tag-order-index-href')) :
                      Ajaxer.post(App.href('tag-order-href')) ;
        ajax.send(this.state.data)
            .end((err, res) => {
                if (Ajaxer.evalError(err)) return;
                Ajaxer.evalJsonResponse(res);
            });
    },
    validateLeaseStartYear() {
        return this.valid(this.isValidYear(this.state.data.leaseStart.year));
    },
    validateLeaseStartMonth() {
        return this.valid(this.isValidMonth(this.state.data.leaseStart.month));
    },
    handleLeaseStartYearChange() {
        const year = this.numberFromRef('leaseStartYear');
        if (this.isValidYear(year)) {
            this.state.data.leaseStart.year = year;
            this.setState({ leaseStart: this.state.data.leaseStart });
        }
    },
    handleLeaseStartMonthChange() {
        const month = this.numberFromRef('leaseStartMonth');
        if (this.isValidMonth(month)) {
            this.state.data.leaseStart.month = month;
            this.setState({ leaseStart: this.state.data.leaseStart });
        }
    },
    validateCount() {
        return this.valid(this.state.data.count > 0);
    },
    handleCountChange() {
        const count = this.numberFromRef('count');
        this.state.data.count = count;
        this.state.data.totalPrice = count * this.state.tagPrice;
        this.setState({  });
    },
    validateTotalPrice() {
        return this.valid(this.state.data.totalPrice > 0);
    },
    handleTotalPriceChange() {
        const totalPrice = this.numberFromRef('totalPrice');
        this.state.data.totalPrice = totalPrice
        this.setState({  });
    },
    render() {
        return (
            <div>
              <form>
                <Input type="number"
                  value={this.state.data.count}
                  placeholder="購入したタグの個数"
                  label="購入したタグの個数"
                  help="必ず入力してください。"
                  bsStyle={this.validateCount()}
                  hasFeedback
                  ref="count"
                  labelClassName={this.labelClassName}
                  wrapperClassName={this.wrapperClassName}
                  onChange={this.handleCountChange}
                />
                <Input type="number"
                  value={this.state.data.totalPrice}
                  placeholder="購入金額"
                  label={"購入金額(単価" + this.state.tagPrice + ")"}
                  help="必ず入力してください。"
                  bsStyle={this.validateTotalPrice()}
                  hasFeedback
                  ref="totalPrice"
                  labelClassName={this.labelClassName}
                  wrapperClassName={this.wrapperClassName}
                  onChange={this.handleTotalPriceChange}
                />
                <Input label="リース開始年月"
                       wrapperClassName="col-xs-offset-2 wrapper"
                       labelClassName={this.labelClassName}
                >
                  <Row>
                    <Col xs={3}>
                      <Input type="number"
                        value={this.state.data.leaseStart.year}
                        min={this.minYear}
                        placeholder="年"
                        bsStyle={this.validateLeaseStartYear()}
                        hasFeedback
                        ref="leaseStartYear"
                        labelClassName={this.labelClassName}
                        wrapperClassName={this.wrapperClassName}
                        onChange={this.handleLeaseStartYearChange}
                      />
                    </Col>
                    <Col xs={3}>
                      <Input type="number"
                        value={this.state.data.leaseStart.month}
                        min={1} max={12}
                        placeholder="月"
                        bsStyle={this.validateLeaseStartMonth()}
                        hasFeedback
                        ref="leaseStartMonth"
                        labelClassName={this.labelClassName}
                        wrapperClassName={this.wrapperClassName}
                        onChange={this.handleLeaseStartMonthChange}
                      />
                    </Col>
                  </Row>
                </Input>
                <ButtonToolbar>
                  <button type="button" className="btn btn-success btn-imp" onClick={this.save}>
                    <span className="glyphicon glyphicon-ok"/>
                    保存
                  </button>
                  <a className="btn btn-default btn-imp" href={App.href('tag-order-index-href')}>
                    <Glyphicon glyph="arrow-left"/>
                    キャンセル
                  </a>
                </ButtonToolbar>
              </form>
            </div>
        );
    },
});

$(() => {
    ReactDOM.render(<Page />, document.getElementById('page'));
});

