'use strict';

import Ajaxer from './lib/ajaxer.js';
import App    from './lib/app.js';

$(() => {
    $('.delete-button').click(function() {
        App.doubleConfirm(
            'タグ購入履歴を削除しますが、よろしいですか？',
            'この操作は取り消せません。本当に削除しますか？',
            () => {
                Ajaxer.del($(this).find('a.hidden').attr('href'))
                    .end((err, res) => {
                        if (Ajaxer.evalError(err)) return;
                        Ajaxer.evalJsonResponse(res);
                    });
            }
        );
    });
});
