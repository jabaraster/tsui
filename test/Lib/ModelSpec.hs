module Lib.ModelSpec (spec) where

import TestImport

import Lib.Model

spec :: Spec
spec = do
    describe "YearMonth" $ do
        ym1 <- return $ YearMonth 2000 10
        ym2 <- return $ YearMonth 2010 10
        it "ym1 < ym1" $ do
            ym1 < ym1 `shouldBe` False
        it "ym1 < ym2" $ do
            ym1 < ym2 `shouldBe` True
        it "ym2 < ym1" $ do
            ym2 < ym1 `shouldBe` False
        it "ym1 <= ym1" $ do
            ym1 <= ym1 `shouldBe` True
        it "ym1 <= ym2" $ do
            ym1 <= ym2 `shouldBe` True
        it "ym2 <= ym2" $ do
            ym2 <= ym1 `shouldBe` False
        it "YearMonth {year = 2016, month = 3} < YearMonth {year = 2015, month = 12}" $ do
            YearMonth {year = 2016, month = 3} < YearMonth {year = 2015, month = 12} `shouldBe` False

    describe "diffYM" $ do
        it "YearMonth 2015 10 `diffYM` YearMonth 2017 3" $ do
            YearMonth 2015 10 `diffYM` YearMonth 2017 3 `shouldBe` 18
        it "YearMonth 2017 3 `diffYM` YearMonth 2015 10" $ do
            YearMonth 2017 3 `diffYM` YearMonth 2015 10 `shouldBe` 18
        it "YearMonth 2015 3 `diffYM` YearMonth 2015 10" $ do
            YearMonth 2015 3 `diffYM` YearMonth 2015 10 `shouldBe` 8
        it "YearMonth 2015 10 `diffYM` YearMonth 2015 3" $ do
            YearMonth 2015 10 `diffYM` YearMonth 2015 3 `shouldBe` 8
        it "YearMonth 2015 10 `diffYM` YearMonth 2015 10" $ do
            YearMonth 2015 10 `diffYM` YearMonth 2015 10 `shouldBe` 1

    describe "addYear" $ do
        it "addYear $ YearMonth 2015 1" $ do
            addYear 1 (YearMonth 2015 12) `shouldBe` YearMonth 2016 12
