module Lib.TsuiSpec (spec) where

import TestImport

import Lib.Model
import Lib.Tsui

spec :: Spec
spec = do
    describe "leaseMonths'" $ do
        it "leaseMonths' (YearMonth 2015 12) 7 2015" $ do
            leaseMonths' (YearMonth 2015 12) 7 2015 `shouldBe` 4

