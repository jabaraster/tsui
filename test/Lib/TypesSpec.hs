module Lib.TypesSpec (spec) where

import TestImport

import Lib.Model (YearMonth(..))
import Lib.Types

spec :: Spec
spec = do
    describe "schoolYearLastMonth" $ do
        it "schoolYearLastMonth 2015" $ do
            schoolYearLastMonth 2015 `shouldBe` YearMonth 2016 3
