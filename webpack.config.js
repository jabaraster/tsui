var webpack = require('webpack');

module.exports = {
  context: __dirname + '/js',
  entry: {
    'app-nav': './app-nav.jsx',
    'cost-parameters-edit': './cost-parameters-edit.jsx',
    'home': './home.jsx',
    'sandbox': './sandbox.jsx',
    'school-edit': './school-edit.jsx',
    'school-index': './school-index.jsx',
    'tag-order-edit': './tag-order-edit.jsx',
    'tag-order-index': './tag-order-index.jsx'
  },
  output: {
    path: __dirname + '/static/js',
    filename: '[name].js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  devtool: 'inline-source-map',
  plugins: [],
  module: {
    loaders: [
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          cacheDirectory: true,
          presets: ['es2015', 'react']
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          cacheDirectory: true,
          presets: ['es2015']
        }
      },
      {
        test: /\.html$/,
        loader: 'file?name=[name].[ext]'
      }
    ]
  }
};
